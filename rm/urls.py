from django.conf.urls import url
from django.shortcuts import render

from . import views

app_name = 'rm'

urlpatterns = [
	url(r'^$', views.index_view, name='index'),

	url(r'^trashes/$', views.trash_list_view, name='trashes'),
	url(r'^trashes/(?P<pk>[0-9]+)/$', views.trash_detail_view, name='trash_detail'),
	url(r'^trashes/create/$', views.trash_create_view, name='trash_create'),
	url(r'^trashes/delete/$', views.trash_delete, name='trash_delete'),
	
	url(r'^operations/$', views.operation_list_view, name='operations'),
	url(r'^operations/(?P<pk>[0-9]+)/$', views.operation_detail_view, name='operation_detail'),
	url(r'^operations/create/$', views.operation_create_view, name='operation_create'),
	url(r'^operations/delete/$', views.operation_delete, name='operation_delete'),
	url(r'^operations/restore/(?P<pk>[0-9]+)/$', views.operation_restore_view, name='operation_restore'),
]