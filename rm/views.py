# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse

from .models import Trash, Operation
from .forms import TrashForm, OperationForm, OperationRestoreForm

from .helpers.trash_helper import create_trash, delete_trash
from .helpers.operation_helper import create_operation, delete_operation, restore_operation, update_operation


def index_view(request):
    return render(request, 'rm/index.html', {})


def trash_list_view(request):
    for operation in Operation.objects.all():
        update_operation(operation)

    return render(request, 'rm/trashes.html', {'trashes': Trash.objects.all()})


def trash_detail_view(request, pk):
    for operation in Operation.objects.all():
        update_operation(operation)

    trash = get_object_or_404(Trash, pk=pk)
    return render(request, 'rm/trash_detail.html', {'trash': trash})


def trash_create_view(request):
    if request.method == 'POST':
        form = TrashForm(request.POST)
        try:
            if form.is_valid():
                create_trash(form=form)
                return HttpResponseRedirect(reverse('rm:trashes'))
            else:
                return render(request, 'rm/error.html', {'message': form.errors})
        except Exception as exc:
            return render(request, 'rm/error.html', {'message': exc.message})
    
    form = TrashForm()
    return render(request, 'rm/trash_create.html', {'form': form})


def trash_delete(request):
    if request.method == 'POST':
        trash = Trash.objects.get(pk=request.POST['pk'])
        delete_trash(trash)
        return HttpResponseRedirect(reverse('rm:trashes'))

    raise Http404()


def operation_list_view(request):
    for operation in Operation.objects.all():
        update_operation(operation)

    return render(request, 'rm/operations.html', {'operations': Operation.objects.all()})


def operation_detail_view(request, pk):
    update_operation(get_object_or_404(Operation, pk=pk))
    operation = get_object_or_404(Operation, pk=pk)
    return render(request, 'rm/operation_detail.html', {'operation': operation})


def operation_create_view(request):
    for operation in Operation.objects.all():
        update_operation(operation)

    if request.method == 'POST':
        form = OperationForm(request.POST)
        try:
            if form.is_valid():
                create_operation(form)
                return HttpResponseRedirect(reverse('rm:operations')) 
            else:
                return render(request, 'rm/error.html', {'message': form.errors})
        except Exception as exc:
            return render(request, 'rm/error.html', {'message': exc.message})

    form = OperationForm()
    return render(request, 'rm/operation_create.html', {'form': form})


def operation_delete(request):
    if request.method == 'POST':
        update_operation(get_object_or_404(Operation, pk=request.POST['pk']))
        delete_operation(get_object_or_404(Operation, pk=request.POST['pk']))
        return HttpResponseRedirect(reverse('rm:operations'))

    raise Http404()


def operation_restore_view(request, pk):
    update_operation(get_object_or_404(Operation, pk=pk))

    if request.method == 'POST':
        form = OperationRestoreForm(request.POST)
        if form.is_valid():
            operation = get_object_or_404(Operation, pk=pk)
            try:
                restore_operation(operation, form)
            except Exception as exc:
                return render(request, 'rm/error.html', {'message': exc.message})
            return HttpResponseRedirect(reverse('rm:operations'))

    operation = get_object_or_404(Operation, pk=pk)
    form = OperationRestoreForm()
    return render(request, 'rm/operation_restore.html', {'form': form, 'operation': operation})
