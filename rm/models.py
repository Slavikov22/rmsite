# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os.path

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone


class Trash(models.Model):
	path = models.CharField(max_length=100)
	maxtime = models.DurationField()

	def __unicode__(self):
		return os.path.basename(self.path.rstrip('/'))


class Operation(models.Model):
	id = models.CharField(max_length=30, primary_key=True)
	name = models.CharField(max_length=100)
	trash = models.ForeignKey(Trash, on_delete=models.CASCADE)
	create_time = models.DateTimeField()

	def __unicode__(self):
		return self.name


class TrashFile(models.Model):
	operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
	old_path = models.CharField(max_length=100)

	def __unicode__(self):
		return os.path.basename(self.old_path.rstrip('/'))
