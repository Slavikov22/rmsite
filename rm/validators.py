import os
import os.path

from django.core.exceptions import ValidationError

from .models import Trash, Operation


def trash_path_validator(path):
	if not os.path.isabs(path):
		raise ValidationError('Invalid path')

	for trash in Trash.objects.all():
		if path.startswith(trash.path) or trash.path.startswith(path):
			raise ValidationError('Intersecting directories')


def operation_path_validator(path):
	if not os.path.isabs(path):
		raise ValidationError('Invalid path')

	if not os.path.exists(path):
		raise ValidationError('File is not exists')

	if not os.access(path, os.R_OK):
		raise ValidationError('Access denied')


def operation_name_validator(name):
	for operation in Operation.objects.all():
		if name == operation.name:
			raise ValidationError('Name is busy')
			