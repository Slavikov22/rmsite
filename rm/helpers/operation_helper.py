from django.utils import timezone

import os.path

import smartrm.rm
from smartrm.operation import load_operations, save_operations
from smartrm.operation import Operation as SmartrmOperation

from ..models import Trash, Operation, TrashFile
from .trash_helper import get_smartrm_trash


def get_smartrm_operation(operation):
    return [ oper for oper in load_operations()
             if str(oper.id) == operation.id ][0]


def create_operation(form):
    smartrm.rm.operation = SmartrmOperation()

    path = form.cleaned_data['path']
    name = form.cleaned_data['name'] if form.cleaned_data['name'] else path
    trash = form.cleaned_data['trash']
    regex = form.cleaned_data['regex']
    create_time = timezone.now()

    if path.startswith(trash.path) or trash.path.startswith(path):
        raise Exception('Invalid path')

    smartrm_trash = get_smartrm_trash(trash)

    if not regex:
        smartrm.rm.remove(path, trash=smartrm_trash)
    else:
        smartrm.rm.remove_from_regex(path, regex, trash=smartrm_trash)

    smartrm.rm.operation.trash_path = trash.path

    id = smartrm.rm.operation.id

    operation = Operation.objects.create(
        id=id, 
        name=name, 
        trash=trash,
        create_time=create_time,
    )

    for file in smartrm.rm.operation.files:
        TrashFile.objects.create(operation=operation, old_path=file.path)

    operations = load_operations()
    operations.append(smartrm.rm.operation)
    save_operations(operations)


def delete_operation(operation):
    smartrm_operation = get_smartrm_operation(operation)
    smartrm_trash = get_smartrm_trash(operation.trash)

    for file in smartrm_operation.files:
        smartrm_trash.delete(file.name, file.index)

    operation.delete()


def restore_operation(operation, form):
    politic = form.cleaned_data['politic']

    if politic == 'safe':
        for file in operation.trashfile_set.all():
            if os.path.exists(file.old_path):
                raise Exception('Find collisions')

    smartrm_operation = get_smartrm_operation(operation)
    smartrm_trash = get_smartrm_trash(operation.trash, politic)

    smartrm.rm.restore(
        operation=smartrm_operation, 
        trash=smartrm_trash, 
        dry_run=False
    )

    operation.delete()


def update_operation(operation):
    smartrm_trash = get_smartrm_trash(operation.trash)

    operations = load_operations()

    if operation.trash.maxtime.total_seconds() > 0:
        if timezone.now() - operation.create_time > operation.trash.maxtime:
            for count in range(len(operations)):
                if str(operations[count].id) == operation.id:
                    del operations[count]
                    break
            operation.delete()

    save_operations(operations)
