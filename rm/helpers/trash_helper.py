import os
import os.path
import shutil

from datetime import timedelta

import smartrm.config

from smartrm.trash import Trash as SmartrmTrash
from smartrm.rmerror import RMError 

from ..models import Trash


def get_smartrm_trash(trash, politic='passive'):
	config = smartrm.config.get_default()

	config.trash_path = trash.path
	config.choise_politic = politic
	config.trash_politics['maxtime'] = trash.maxtime.total_seconds()

	return SmartrmTrash(config=config)


def create_trash(form):
	path = form.cleaned_data['path']

	maxtime = 0

	if form['maxtime']:
		maxtime = form.cleaned_data['maxtime_hours']*60*60 +\
	              form.cleaned_data['maxtime_minutes']*60 +\
	              form.cleaned_data['maxtime_seconds']

	trash = Trash.objects.create(path=path, maxtime=timedelta(seconds=maxtime))

	try:
		get_smartrm_trash(trash)
	except Exception as exc:
		trash.delete()
		raise


def delete_trash(trash):
	shutil.rmtree(trash.path)
	trash.delete()
