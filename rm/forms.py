from django import forms

from .models import Trash
from .validators import trash_path_validator, operation_path_validator, operation_name_validator


class TrashForm(forms.Form):
	path = forms.CharField(max_length=100, validators=[trash_path_validator,])

	maxtime = forms.BooleanField(required=False)
	maxtime_hours = forms.IntegerField(min_value=0, max_value=999, initial=0)
	maxtime_minutes = forms.IntegerField(min_value=0, max_value=59, initial=0)
	maxtime_seconds = forms.IntegerField(min_value=0, max_value=59, initial=0)


class OperationForm(forms.Form):
	name = forms.CharField(max_length=30, required=False, validators=[operation_name_validator,])
	path = forms.CharField(max_length=100, validators=[operation_path_validator,])
	trash = forms.ModelChoiceField(queryset=Trash.objects.all())
	regex = forms.CharField(max_length=30, required=False)


class OperationRestoreForm(forms.Form):
	politic = forms.ChoiceField(choices=[('safe', 'Safe'), ('agressive', 'Agressive')])
